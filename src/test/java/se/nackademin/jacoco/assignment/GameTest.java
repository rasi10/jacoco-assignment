package se.nackademin.jacoco.assignment;

import org.junit.Test;

import junit.framework.Assert;
import se.nackademin.main.Main;




public class GameTest{	
	
	@Test
	public void testMethod1(){
		Game game = new Game();
		int i = game.calculateOutPutBasedOnNames("Rafael","Silva");
		Assert.assertEquals(i,1);		
	}
	
	@Test
	public void testMethod2(){
		Game game = new Game();
		game.calculateOutPutBasedOnNames("silva","rafael");
	}
	
	@Test
	public void testMethod4(){
		Game game = new Game();
		game.calculateOutPutBasedOnGender('M');
	}

//	
//	@Test
//	public void testMethod3(){
//		Game game = new Game();
//		game.runGame("game", "Rafael","Silva",'M',30,"fortaleza");
//		
//	}
	
	
}

